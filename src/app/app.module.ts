import {ModuleWithProviders, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NgStoryblokConfiguration} from "../config/ng-storyblok-configuration";
import {ConfigService} from "./services/config.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  static forRoot(config: NgStoryblokConfiguration): ModuleWithProviders<AppModule> {
    return {
      ngModule: AppModule,
      providers: [
        {
          provide: ConfigService,
          useValue: new ConfigService(config)
        }
      ]
    }
  }
}
