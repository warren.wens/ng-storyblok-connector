import {Injectable} from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from 'rxjs';
import {Story} from "../../models/storyblok/story";

@Injectable({
  providedIn: 'root'
})
export class StoryblokClientService {
  private accessToken: string;

  constructor(private configService: ConfigService, private http: HttpClient) {
    this.accessToken = configService.getConfig().accessToken;
  }

  getStory(path: string): Observable<Story> {
    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');

    return this.http.get<Story>(`${this.configService.getDomain()}/${path}?token=${this.accessToken}`, {
      headers
    });
  }
}
