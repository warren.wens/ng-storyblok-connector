import { Injectable } from '@angular/core';
import {NgStoryblokConfiguration} from "../../config/ng-storyblok-configuration";

const DEFAULT_DOMAIN = 'https://api.storyblok.com';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private _config: NgStoryblokConfiguration) {}

  getConfig(): NgStoryblokConfiguration {
    return this._config;
  }

  getDomain(): string {
    return this._config.domain || DEFAULT_DOMAIN;
  }
}
