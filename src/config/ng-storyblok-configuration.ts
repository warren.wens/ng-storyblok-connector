export interface NgStoryblokConfiguration {
  accessToken: string;
  domain?: string;
}
