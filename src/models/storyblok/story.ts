import {Alternate} from "./Alternate";
import {TranslatedSlug} from "./translated-slug";

export interface Story {
  id: number;
  uuid: string;
  name: string;
  slug: string;
  full_slug: string;
  default_full_slug: string|null;
  created_at: Date;
  published_at: Date;
  first_published_at: Date;
  release_id: number|null;
  lang: string;
  content: object;
  position: number;
  is_startpage: boolean;
  parent_id: number;
  group_id: string,
  alternates: Alternate[],
  translated_slugs: TranslatedSlug[],
  links: string[],
  rels: Story[],
  sort_by_date: Date,
  tag_list: string[],
  meta_data: object,
  path: string
}
