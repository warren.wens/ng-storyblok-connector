export interface TranslatedSlug {
  path: string;
  name: string;
  lang: string;
}
