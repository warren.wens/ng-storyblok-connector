export interface Alternate {
  id: number;
  name: string;
  slug: string;
  full_slug: string;
  is_folder: boolean;
  parent_id: number;
}
